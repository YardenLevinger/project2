#ifndef PROJECT2_MYTESTCLIENTHANDLERFACTORY_H
#define PROJECT2_MYTESTCLIENTHANDLERFACTORY_H

#include "../Algorithms/AStarSearch.h"
#include "./IClientHandlerFactory.h"
#include "./MyTestClientHandler.h"
#include <memory>

template <class SolverType>
class MyTestClientHandlerFactory: public IClientHandlerFactory{
public:
    // shared_ptr<ClientHandler> createHandler() override;
    ClientHandler* createHandler() override;

};

#endif //PROJECT2_MYTESTCLIENTHANDLERFACTORY_H
