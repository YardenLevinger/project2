#include "MyTestClientHandlerFactory.h"


/*************************************************************************
 * Function Name: createHandler
 * Input: non
 * output: an instance of ClientHandler.
 * Function operation: create new instance of ClientHandler and returns it.
*************************************************************************/
template <class SolverType>
ClientHandler* MyTestClientHandlerFactory<SolverType>::createHandler() {
    return new MyTestClientHandler<SolverType>();
}

template class MyTestClientHandlerFactory<AStarSearch>;