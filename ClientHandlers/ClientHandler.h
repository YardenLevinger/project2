#ifndef PROJ2_CLIENTHANDLER_H
#define PROJ2_CLIENTHANDLER_H

#include <istream>

using namespace std;
class ClientHandler
{
  public:
    virtual string handleClient(char *buffer, int numOfBytes) = 0;
    virtual ~ClientHandler() {}
};

#endif //PROJ2_CLIENTHANDLER_H
