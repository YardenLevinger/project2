
#ifndef PROJ2_MYTESTCLIENTHANDLER_H
#define PROJ2_MYTESTCLIENTHANDLER_H

#include "../Interfaces/Solver.h"
#include "./ClientHandler.h"
#include "../Interfaces/ICacheManager.h"
#include "../Communication/CommunicationChannel.h"
#include "../Caching/FileCacheManeger.h"
#include "../Algorithms/Infrastructure.h"
//#include <iostream>
#include "../Algorithms/AStarSearch.h"
#include <algorithm>

using namespace std;
using namespace Searchers;

template <class SolverType>
class MyTestClientHandler: public ClientHandler{
private:
    Solver<problem_args,string>* mySolver;
    shared_ptr<FileCacheManager> fileCacheManager;
    vector<string> problemLines;
    problem_args* args;

public:
    MyTestClientHandler();
    string handleClient(char * buffer, int numOfBytes) override;
    virtual ~MyTestClientHandler() override;
};

#endif //PROJ2_MYTESTCLIENTHANDLER_H
