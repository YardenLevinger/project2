#include "MyTestClientHandler.h"

template <class SolverType>
MyTestClientHandler<SolverType>::MyTestClientHandler()
{
    if (is_base_of<Solver<problem_args, string>, SolverType>::value)
    {
        this->fileCacheManager = FileCacheManager::getInstance();
        this->mySolver = new SolverType();
    }
}

/*************************************************************************
 * Function Name: handleClient
 * Input: message from the server(that was recived from the client),
 * and its length.
 * output: the message(problem) solution.
 * Function operation: getting the ful problem from the server, then checking
 * whether the solution exists in the cache, if so- then return the solution from the cache,
 * otherwise- solve the problem and return the solution.
*************************************************************************/
template <class SolverType>
string MyTestClientHandler<SolverType>::handleClient(char *buffer, int numOfBytes)
{
    istringstream currentBufferStr(string(buffer, numOfBytes));
    string currentLine;
    while (getline(currentBufferStr, currentLine))
    {
        if (currentLine.find("end") == string::npos)
        {
            if (!currentLine.empty())
            {
                this->problemLines.push_back(currentLine);
            }
        }
        else
        {

            string problem = "";
            for (vector<string>::iterator it = this->problemLines.begin(); it != this->problemLines.end(); ++it)
            {
                problem += *it + "\n";
            }
            if (this->fileCacheManager->isSolutionExists(problem))
            {
                return this->fileCacheManager->getSolution(problem);
            }

            int vecSize = this->problemLines.size();
            int numOfLines = vecSize - 2;
            int numOfColumns = count(this->problemLines[0].begin(), this->problemLines[0].end(), ',') + 1;

            string end_point = this->problemLines[vecSize - 1];
            string start_point = this->problemLines[vecSize - 2];
            this->problemLines.pop_back();
            this->problemLines.pop_back();
            int **mat = new int *[numOfLines];
            int j, i = 0;
            //cout<<"numOfLines: "<<numOfLines<<endl;
            //cout<<"numOfColumns: "<<numOfColumns<<endl;
            for (vector<string>::iterator it = this->problemLines.begin(); it != this->problemLines.end(); ++it)
            {
                mat[i] = new int[numOfColumns];
                istringstream currentLineStream(*it);
                string currentNumber;
                j = 0;
                while (getline(currentLineStream, currentNumber, ','))
                {
                    try {
                    mat[i][j] = stoi(currentNumber);
                        j++;

                    }
                    catch(...) {
                    }

                }
                ++i;
            }

            this->args = new problem_args();
            this->args->mat = mat;
            this->args->start_row = stoi(start_point.substr(0, start_point.find(",")));
            this->args->start_col = stoi(start_point.substr(start_point.find(",") + 1));

            this->args->end_row = stoi(end_point.substr(0, end_point.find(",")));
            this->args->end_col = stoi(end_point.substr(end_point.find(",") + 1));
            this->args->rows = numOfLines;
            this->args->cols = numOfColumns;

            string solution = this->mySolver->solve(this->args);

            this->fileCacheManager->saveSolution(problem, solution);

            /*
            for(int i = 0 ; i < numOfLines ; i++){
                delete [] mat[i];
            }
            delete [] mat;

            delete (args);
*/
            return solution;
        }
    }
    return "";
}

template <class SolverType>
MyTestClientHandler<SolverType>::~MyTestClientHandler()
{
    delete (this->mySolver);
    for (int i = 0; i < this->args->rows; i++)
    {
        delete[] this->args->mat[i];
    }
    delete[] this->args->mat;
    delete (this->args);
}

template class MyTestClientHandler<AStarSearch>;