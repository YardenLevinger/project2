#ifndef PROJECT2_ICLIENTHANDLERFACTORY_H
#define PROJECT2_ICLIENTHANDLERFACTORY_H

#include "ClientHandler.h"
#include <memory>

class IClientHandlerFactory{
public:
    // virtual shared_ptr<ClientHandler> createHandler()=0;
    virtual ClientHandler* createHandler()=0;
};

#endif //PROJECT2_ICLIENTHANDLERFACTORY_H
