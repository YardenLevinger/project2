#ifndef PROJ2_COMMUNICATIONCHANNEL_H
#define PROJ2_COMMUNICATIONCHANNEL_H

#include "../Interfaces/ICommunicationChannel.h"
using namespace std;

class CommunicationChannel:public virtual ICommunicationChannel{
protected:
    int socketID;
//    char buffer[DEFAULT_BUFLEN];
public:
    void readBuffer(int &numOfBytesRecieved, int socketID, int flag, char* buffer)override;
    int sendBuffer(const char *buffer, int length, int socketID) override;
    virtual void closeSocket(int socketID);// { close(this->socketID); }
};

#endif //PROJ2_COMMUNICATIONCHANNEL_H
