#include "CommunicationChannel.h"
#include <iostream>

/*************************************************************************
 * Function Name: readBuffer
 * Input: message length, the client socket number, flag and the message
 * output: non
 * Function operation: using recv to read the client message and upadating
 * the length of the message.
*************************************************************************/
void CommunicationChannel::readBuffer(int &numOfBytesRecieved, int socketID, int flag, char *buffer)
{
    numOfBytesRecieved = recv(socketID, buffer, DEFAULT_BUFLEN, flag);
}

/*************************************************************************
 * Function Name: sendBuffer
 * Input: message length, the client socket number and the message
 * output: the message length.
 * Function operation: using write to send the message to the client.
*************************************************************************/
int CommunicationChannel::sendBuffer(const char *buffer, int length, int socketID)
{

    if (socketID == -1)
        socketID = this->socketID;

    int ans = write(socketID, buffer, length);
    return ans;
}

/*************************************************************************
 * Function Name: closeSocket
 * Input: the client socket number.
 * output: non
 * Function operation: using close function to close the socket.
*************************************************************************/
void CommunicationChannel::closeSocket(int socketID)
{
    //shutdown(socketID,SHUT_RD);
    close(socketID);
    //cout<<"ShutDown!!!!"<<endl;
}
