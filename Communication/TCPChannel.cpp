#include "TCPChannel.h"

/*************************************************************************
 * Function Name: TcpChannel
 * Input: non
 * output: non
 * Function operation: creating the server socket number.
*************************************************************************/
TCPchannel::TCPchannel(){
    /* First call to socket() function */
    this->socketID = socket(AF_INET, SOCK_STREAM, 0);

    if (this->socketID < 0)
    {
        perror("ERROR opening socket");
        exit(1);
    }
}
