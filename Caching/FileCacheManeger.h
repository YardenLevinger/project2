#ifndef PROJ2_FILECHACHEMANEGER_H
#define PROJ2_FILECHACHEMANEGER_H

#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>
#include <mutex>
#include <map>
#include <memory>
#include "../Interfaces/ICacheManager.h"


class FileCacheManager:public IcacheManager{
private:
    mutex map_mutex;
    mutex file_mutex;
    map<string,string> problemToSolutionMap;
    static shared_ptr<FileCacheManager> instance;

protected:
    FileCacheManager(){
        this->fillMap();
    }
/*************************************************************************
 * Function Name: fillMap
 * Input: non
 * output: non
 * Function operation: reading the problems and solutions from the file and
 * initializing the map.
*************************************************************************/
    void fillMap(){
        ifstream readingFile("Cache.txt");
        if(readingFile.is_open()) {
            string problem, solution;
            while(getline(readingFile,problem, '$')) {
                if(!problem.empty()) {
                    getline(readingFile, solution, '$');
                    this->problemToSolutionMap[problem] = solution;
                }
            }
            readingFile.close();
        }
    }

public:

/*************************************************************************
 * Function Name: isSolutionExists
 * Input: the problem
 * output: true if the solution is alreadt exists in the cache, false-
 * otherwise.
 * Function operation: if the problem is one of the map's keys then the
 * solution exists and there is no need to solve the problem
*************************************************************************/
    bool isSolutionExists(string problem) override {
        lock_guard<mutex> guard(this->map_mutex);
        return this->problemToSolutionMap.find(problem) != this->problemToSolutionMap.end();
    }
/*************************************************************************
 * Function Name: getSolution
 * Input: the problem
 * output: the solution to the problem.
 * Function operation: finding the value (solution) to the key(problem)
 * that was given as an argument.
*************************************************************************/
    string getSolution(string problem) override {
        lock_guard<mutex> guard(this->map_mutex);
        return this->problemToSolutionMap.at(problem);
    }
/*************************************************************************
 * Function Name: saveSolution
 * Input: the problem and its solution.
 * output: non
 * Function operation: adding another pair of <problem,solution> to the map
 * and at the same time adding it to the file.
*************************************************************************/
    void saveSolution(string problem, string solution) override {
        lock_guard<mutex> guard(this->map_mutex);
        this->problemToSolutionMap[problem] = solution;
        this->loadToFile(problem, solution);
    }
/*************************************************************************
 * Function Name: loadToFile
 * Input: the problem and its solution.
 * output: non
 * Function operation: writing the two to the file.
*************************************************************************/
    void loadToFile(string problem, string solution){
        lock_guard<mutex> guard(this->file_mutex);
        ofstream writingFile("Cache.txt", ios::app);
        writingFile<<(problem+"$"+solution+"$");
    }

/*************************************************************************
 * Function Name: getInstance
 * Input: non
 * output: FileCacheManager.
 * Function operation: creating new object of FileCacheManager and returns it.
*************************************************************************/
    static shared_ptr<FileCacheManager> getInstance(){
        if(instance == nullptr){
            instance = shared_ptr<FileCacheManager>(new FileCacheManager());
        }
        return instance;
    }
};

#endif //PROJ2_FILECHACHEMANEGER_H
