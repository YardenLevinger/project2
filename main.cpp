#include <map>
#include <vector>
#include <cmath>
#include <cstdlib>
#include "./Servers/MyParallelServer.h"

#include "./Algorithms/DepthFirstSearch.h"
#include "./Algorithms/BestFirstSearch.h"
#include "./Algorithms/BreadthFirstSearch.h"
#include "./Algorithms/AStarSearch.h"
#include "./ClientHandlers/MyTestClientHandlerFactory.h"
#define N 50

using namespace Searchers;
using namespace std;

int main(int argc, char *argv[])
{
    //TODO: main, SolverType, dirPath (searcher)

	/*
	srand(time(NULL));
	// int mat[5][5] = {{12, 21, 13, 22, 23}, {55, 26, 19, 23, 23}, {65, 21, 42, 22, 23}, {34, 26, 23, 52, 43}, {12, 21, 13, 22, 23}};
	DepthFirstSearch d;
	BestFirstSearch b;
	BreadthFirstSearch br;
	AStarSearch a;
	problem_args args;
	cout<<25<<endl;

	int **m=new int*[N];

	for(int i=0; i<N; i++)
	{
		m[i]=new int[N];
		for(int j=0; j<N; j++)
		{
			m[i][j]=rand()%100;
		}
	}

	//int *m[5] = {mat[0], mat[1], mat[2], mat[3], mat[4]};
	args.mat = m;
	args.rows = args.cols = N;
	args.end_row = rand()%N;
	args.end_col = rand()%N;
	args.start_col = rand()%N;
	args.start_row = rand()%N;
	cout<<"DepthFirstSearch:"<<endl;
	string s = d.solve(args);
	cout << d.getSum() << endl;
	cout<<"BestFirstSearch:"<<endl;
	s = b.solve(args);
	cout << b.getSum() << endl;
	cout<<"BreadthFirstSearch:"<<endl;
	s = br.solve(args);
	cout<<50<<endl;
	cout << br.getSum() << endl;
	cout<<"AStarSearch:"<<endl;
	s = a.solve(args);
	cout<<52<<endl;
	cout << a.getSum() << endl;
	cout<<"BreadthFirstSearch:"
	// cout << s << endl;
	//cout << br.getSum() << endl;
	cout << endl;
	return 0;
	 */

	MyParallelServer server;
	IClientHandlerFactory* clientHandlerFactory=new MyTestClientHandlerFactory<AStarSearch>();
	server.startListen(stoi(argv[1]),clientHandlerFactory);
	delete(clientHandlerFactory);
}