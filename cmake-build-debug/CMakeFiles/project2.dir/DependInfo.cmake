# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/Algorithms/DepthFirstSearch.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/Algorithms/DepthFirstSearch.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/Caching/FileCacheManager.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/Caching/FileCacheManager.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/ClientHandlers/MyTestClientHandler.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/ClientHandlers/MyTestClientHandler.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/ClientHandlers/MyTestClientHandlerFactory.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/ClientHandlers/MyTestClientHandlerFactory.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/Communication/CommunicationChannel.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/Communication/CommunicationChannel.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/Communication/TCPChannel.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/Communication/TCPChannel.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/Plotting/example.cc" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/Plotting/example.cc.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/Servers/MyParallelServer.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/Servers/MyParallelServer.cpp.o"
  "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/main.cpp" "/home/yarden/Desktop/Study/Advanced Programming/Project/project2/cmake-build-debug/CMakeFiles/project2.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../."
  "../Algorithms"
  "../Caching"
  "../ClientHandlers"
  "../Communication"
  "../Interfaces"
  "../Plotting"
  "../Servers"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
