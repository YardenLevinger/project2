#ifndef PROJ2_ISERVER_H
#define PROJ2_ISERVER_H

#include "ICommunicationChannel.h"
#include <bits/socket.h>
#include "../ClientHandlers/ClientHandler.h"
#include "../ClientHandlers/IClientHandlerFactory.h"

namespace server_side{
    class IServer:public virtual ICommunicationChannel{
    public:
        virtual int startListen(int port, IClientHandlerFactory* clientHandlerFactory) = 0;
        virtual void stop() = 0;
    };
}

#endif //PROJ2_ISERVER_H
