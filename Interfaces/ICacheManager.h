
#ifndef PROJ2_CACHEMANAGER_H
#define PROJ2_CACHEMANAGER_H

using namespace std;
class IcacheManager{
public:
    virtual bool isSolutionExists(string problem)=0;
    virtual string getSolution(string problem)=0;
    virtual void saveSolution(string problem, string solution)=0;
};

#endif //PROJ2_CACHEMANAGER_H
