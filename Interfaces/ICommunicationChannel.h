

#ifndef PROJ2_ICOMMUNICATIONCHANNEL_H
#define PROJ2_ICOMMUNICATIONCHANNEL_H
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#define DEFAULT_BUFLEN 1024



using namespace std;

class ICommunicationChannel{
public:
    virtual void readBuffer(int &numOfBytesRecieved, int socketID, int flag, char* buffer)= 0;
    virtual int sendBuffer(const char *buffer, int length, int socketID) = 0;
};

#endif //PROJ2_ICOMMUNICATIONCHANNEL_H
