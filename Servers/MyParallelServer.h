
#ifndef PROJ2_MYPARALLELSERVER_H
#define PROJ2_MYPARALLELSERVER_H

#include <vector>
#include <stack>
#include <errno.h>
#include <thread>
#include <iostream>
#include <signal.h>
#include "../ClientHandlers/IClientHandlerFactory.h"
#include "../Interfaces/IServer.h"
#include "../Communication/TCPChannel.h"
using namespace server_side;
//typedef bool (*OnMessegeRecievedCallback)(const char *buffer, int numOfBytesRecieved);

class MyParallelServer : public IServer, public TCPchannel
{
  private:
    struct ClientStruct
    {
        int clientSocketID;
        // shared_ptr<ClientHandler> clientHandler;
        ClientHandler *clientHandler;
        CommunicationChannel *server;
    };
    stack<pthread_t> threads;
    vector<ClientStruct *> clientStructVec;
    static void *continuosListening(void *clientStruct);

  public:
    int startListen(int port, IClientHandlerFactory *clientHandler);

    void stop() override;
};

#endif //PROJ2_MYPARALLELSERVER_H
