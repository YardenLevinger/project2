
#include "MyParallelServer.h"

/*************************************************************************
 * Function Name: startListen
 * Input: the port on which the server listen ans the ClientHandler factory.
 * output: if the listening succeeded or not.
 * Function operation: after binding the server socket to the right address and listening
 * to it, we create the clients's sockets a thread for each client(which means a thread for each
 * problem). after one sec, if there was no connection of a client, then the listening stops.
*************************************************************************/
int MyParallelServer::startListen(int port, IClientHandlerFactory* clientHandlersFactory) {
    struct sockaddr_in serv_addr;
    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);

    if (bind(this->socketID, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR on binding");
        return -1;
    }

    listen(this->socketID, 5);

    int clilen;
    struct sockaddr_in cli_addr;
    clilen = sizeof(cli_addr);

    int currentSocketID;

    timeval timeout;

    bool x = false;
    while(true){
        if (x) break;
         currentSocketID = accept(this->socketID, (struct sockaddr *)&cli_addr, (socklen_t *)&clilen);
         if(currentSocketID < 0){
             if(errno == EWOULDBLOCK){
//                 cout<<"timeout!"<<endl;
                 this->stop();
                 break;
             }
             else{
                 perror("other error");
             }
         } else {
             ClientStruct *clientStruct = new ClientStruct();
             clientStruct->clientSocketID = currentSocketID;
             clientStruct->clientHandler = clientHandlersFactory->createHandler();
             clientStruct->server = this;
             this->clientStructVec.push_back(clientStruct);
             void *structOfClient = (void *) clientStruct;
             pthread_t thread;
             if (pthread_create(&thread, nullptr, MyParallelServer::continuosListening, structOfClient) != 0) {
                 perror("thread failed!!!");
             }
             this->threads.push(thread);
             timeout.tv_sec = 1;
             timeout.tv_usec = 0;
             setsockopt(this->socketID, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(timeout));
         }
    }
}

/*************************************************************************
 * Function Name: continuosListening
 * Input: the client information
 * output: non
 * Function operation: as long as we didnt get any solution, which means
 * we didnt get to the end of the problem, we are keep reading lines from the client.
 * then, we are solving the problem using handleClient function,
 * send it back to the client and closing the client socket.
*************************************************************************/
void* MyParallelServer::continuosListening(void* clientStruct){
    int numOfBytesRecived;
    char* buffer = new char[DEFAULT_BUFLEN];
    string problem;
    string solution;
    ClientStruct * structOfClient = static_cast<ClientStruct*>(clientStruct);
    while(solution.empty()){
        structOfClient->server->readBuffer(numOfBytesRecived, structOfClient->clientSocketID, MSG_WAITFORONE, buffer);
        if(numOfBytesRecived>0)
        {
            solution = structOfClient->clientHandler->handleClient(buffer, numOfBytesRecived);
            bzero(buffer,numOfBytesRecived);
        }
    }
    structOfClient->server->sendBuffer(solution.c_str(), solution.length(), structOfClient->clientSocketID);
    structOfClient->server->closeSocket(structOfClient->clientSocketID);
    delete [] buffer;
    return NULL;
}

/*************************************************************************
 * Function Name: stop
 * Input: non
 * output: non
 * Function operation: "joining" all the threads, freeing all clientStructs
 * we created and closing the server socket.
*************************************************************************/
void MyParallelServer::stop(){
    while(!this->threads.empty()){
        pthread_t t = this->threads.top();
        pthread_join(t, NULL);
        this->threads.pop();
    }
    int vecSize = this->clientStructVec.size();
    for(int i = 0; i<vecSize; i++){
        delete (this->clientStructVec[i]->clientHandler);
        delete (this->clientStructVec[i]);
    }

    //cout<<"Close Sockes: "<<this->socketID<<endl;
    this->closeSocket(this->socketID);
}