#ifndef BEST_FIRST_SEARCH_H
#define BEST_FIRST_SEARCH_H

#include "Enums.h"
#include "../Interfaces/Solver.h"
#include "Infrastructure.h"
#include <string>
#include <queue>
#include <stack>
#include <iostream>
#include "Searcher.h"

using namespace std;

namespace Searchers
{
class BestFirstSearch : public Searcher<graph_node>
{
private:
  static bool compareNodes(graph_node *a, graph_node *b)
  {
    return (a->weigth > b->weigth);
  }
  static void initialze_neighbor(graph_node *current, graph_node *neighbor, problem_args *args)
  {
    return;
  }

public:
  BestFirstSearch():Searcher(&compareNodes,&initialze_neighbor){}
};
} // namespace Searchers

#endif