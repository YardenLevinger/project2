#ifndef ENUMS_H
#define ENUMS_H

enum Direction{
    Up,
    Down,
    Left,
    Right,
    NotSet, 
    Start,
    isInf
};

#endif