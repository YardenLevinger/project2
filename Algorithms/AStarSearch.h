#ifndef ASTAR_SEARCH_H
#define ASTAR_SEARCH_H

#include "Enums.h"
#include "../Interfaces/Solver.h"
#include "Infrastructure.h"
#include <string>
#include <queue>
#include <stack>
#include <iostream>
#include "Searcher.h"

using namespace std;

namespace Searchers
{
class AStar_graph_node : public graph_node
{
  public:
    int weightSoFar;
    int priority;

    AStar_graph_node(int **mat, unsigned char row, unsigned char col) : graph_node(mat, row, col)
    {
        this->weightSoFar = 0;
        this->priority = 0;
    }
    AStar_graph_node() : graph_node()
    {
        this->weightSoFar = 0;
        this->priority = 0;
    }

    bool operator<(const AStar_graph_node &other) const
    {
        return (this->priority > other.priority);
    }
};
class AStarSearch : public Searcher<AStar_graph_node> // : public Solver<problem_args, string>
{
  private:
    static bool compareNodes(AStar_graph_node *a, AStar_graph_node *b)
    {
        return (a->priority > b->priority);
    }
    static void initialze_neighbor(AStar_graph_node *current, AStar_graph_node *neighbor, problem_args *args)
    {
        neighbor->weightSoFar = current->weightSoFar + neighbor->weigth;
        neighbor->priority = neighbor->weightSoFar + huristicFunc(neighbor,args);
    }

    static int huristicFunc(const AStar_graph_node *node, problem_args *args)
    {
        return abs(node->col_index - args->end_col) + abs(node->row_index - args->end_row);
    }

  public:
    AStarSearch() : Searcher(&compareNodes, &initialze_neighbor){}
};
} // namespace Searchers

#endif