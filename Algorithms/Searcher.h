#ifndef SEARCHER_H
#define SEARCHER_H

#include "../Interfaces/Solver.h"
//#include "Infrastructure.h"
#include <string>
#include <queue>
#include <iostream>
using namespace std;
using namespace Searchers;
template <class graph_node_T>
class Searcher : public Solver<problem_args, string>
{
    class comparable_graph_node_T : public graph_node_T
    {
      public:
        bool (*compare)(graph_node_T *, graph_node_T *);
        comparable_graph_node_T() : graph_node_T()
        {
            this->compare = NULL;
        }
        comparable_graph_node_T(int **mat,
                                unsigned char row,
                                unsigned char col,
                                bool (*compare)(graph_node_T *, graph_node_T *)) : graph_node_T(mat, row, col)
        {
            this->compare = compare;
        }
    };

  protected:
    string dirPath;
    problem_args *args;
    comparable_graph_node_T **mat;
    long sum;
    bool (*compare)(graph_node_T *, graph_node_T *);
    void (*initialze_neighbor)(graph_node_T *current, graph_node_T *neighbor, problem_args *args);

    class comparable_graph_node_T_ptr
    {
      public:
        comparable_graph_node_T *node;
        comparable_graph_node_T_ptr(comparable_graph_node_T *node) : node(node) {}
        bool operator<(const comparable_graph_node_T_ptr &other) const
        {
            return this->node->compare(this->node, other.node);
        }
    };

/*************************************************************************
 * Function Name: createGraphNodesMat
 * Function operation: the function create the graph nodes from the matrix
 * in the program args.
*************************************************************************/
    virtual void createGraphNodesMat()
    {
        this->mat = new comparable_graph_node_T *[this->args->rows];
        for (int i = 0; i < this->args->rows; i++)
        {
            this->mat[i] = new comparable_graph_node_T[this->args->cols];
            for (int j = 0; j < this->args->cols; j++)
            {
                this->mat[i][j] = comparable_graph_node_T(this->args->mat, i, j, this->compare);
            }
        }
    }

  public:
    Searcher(bool (*compare)(graph_node_T *, graph_node_T *), void(*initialze_neighbor)(graph_node_T *current, graph_node_T *neighbor, problem_args *args))
    {
        this->dirPath = "";
        this->sum = 0;
        this->initialze_neighbor = initialze_neighbor;
        this->compare = compare;
    }


/*************************************************************************
 * Function Name: saveSolution
 * Input: the relevant program arguments.
 * output: string that represents the lowest cost path from the start to
 * the end.
 * Function operation: calculates the most efficient path (using the
 * Searcher's algorithm from the initialze_neighbor and compare member
 * function.
*************************************************************************/
    virtual string solve(problem_args* args)
    {
        this->args = args;
        this->createGraphNodesMat();

        priority_queue<comparable_graph_node_T_ptr> pqu;
        this->mat[args->start_row][args->start_col].commingFrom = Start;

        pqu.push((&(this->mat[args->start_row][args->start_col])));

        comparable_graph_node_T *current_node;
        comparable_graph_node_T *neighbor;

        while (!pqu.empty())
        {
            //get the next row from the priority queue.
            current_node = pqu.top().node;
            pqu.pop();
            if (current_node->col_index == args->end_col && current_node->row_index == args->end_row) //finished
            {
                break;
            }
            else
            {
                //check the neigbor in the previous rows.
                if (current_node->row_index > 0 && this->mat[current_node->row_index - 1][current_node->col_index].commingFrom == NotSet)
                {
                    neighbor = &this->mat[current_node->row_index - 1][current_node->col_index];
                    neighbor->commingFrom = Down;
                    this->initialze_neighbor(current_node, neighbor, this->args);
                    pqu.push(neighbor);
                }

                //check the neigbor in the next row.
                if (current_node->row_index < args->rows - 1 && this->mat[current_node->row_index + 1][current_node->col_index].commingFrom == NotSet)
                {
                    neighbor = &this->mat[current_node->row_index + 1][current_node->col_index];
                    neighbor->commingFrom = Up;
                    this->initialze_neighbor(current_node, neighbor, this->args);
                    pqu.push(neighbor);
                }

                //check the neigbor in the previous col.
                if (current_node->col_index > 0 && this->mat[current_node->row_index][current_node->col_index - 1].commingFrom == NotSet)
                {
                    neighbor = &this->mat[current_node->row_index][current_node->col_index - 1];
                    neighbor->commingFrom = Right;
                    this->initialze_neighbor(current_node, neighbor, this->args);
                    pqu.push(neighbor);
                }

                //check the neigbor in the next col.
                if (current_node->col_index < args->cols - 1 && this->mat[current_node->row_index][current_node->col_index + 1].commingFrom == NotSet)
                {
                    neighbor = &this->mat[current_node->row_index][current_node->col_index + 1];
                    neighbor->commingFrom = Left;
                    this->initialze_neighbor(current_node, neighbor, this->args);
                    pqu.push(neighbor);
                }
            }
        }

        if (! (current_node->col_index == args->end_col && current_node->row_index == args->end_row)) //if not arrived to end
        {
            return "-1";
        }

        bool arrivedStart = false;
        this->sum = 0;
        this->dirPath = "";
        string currentDir;
        while (!arrivedStart)
        {
            // if there is no path to the end
            if(current_node->commingFrom==NotSet)
            {
                return "-1";
            }

            this->sum += current_node->weigth;
            Direction commingFrom=current_node->commingFrom;
            current_node->commingFrom=NotSet;
            switch (commingFrom)
            {
            case Up:
                current_node = &mat[current_node->row_index - 1][current_node->col_index];
                currentDir = "Down, ";
                break;
            case Down:
                current_node = &mat[current_node->row_index + 1][current_node->col_index];
                currentDir = "Up, ";
                break;
            case Left:
                current_node = &mat[current_node->row_index][current_node->col_index - 1];
                currentDir = "Right, ";
                break;
            case Right:
                current_node = &mat[current_node->row_index][current_node->col_index + 1];
                currentDir = "Left, ";
                break;
            case Start:
                arrivedStart = true;
                currentDir="";
                break;
            }
//            cout<<" i = "<<(int)current_node->row_index<<" j = "<<(int)current_node->col_index<<"  dir = "<<currentDir<<endl;
            this->dirPath.insert(0, currentDir);
        }
        this->dirPath = this->dirPath.substr(0, this->dirPath.length() - 2);
        return this->dirPath;
    }

    long getSum() { return this->sum; }

    virtual string getPath() { return this->dirPath; }
    virtual ~Searcher()
    {
        for (int i = 0; i < this->args->rows; i++)
        {
            delete [] this->mat[i];
        }
        delete [] this->mat;
    }
};

#endif