#include "DepthFirstSearch.h"
#define MIN(a, b) (((a) < (b)) ? (a) : (b))
#define MAX(a, b) (((a) > (b)) ? (a) : (b))
using namespace Searchers;
using namespace std;

string DepthFirstSearch::solve(problem_args args)
{
    this->sum = 0;
    this->dirPath = "";
    this->args = &args;
    this->createGraphNodesMat();
    this->recursiveDFS(&this->mat[args.start_row][args.start_col]);

    this->dirPath = this->dirPath.substr(0, this->dirPath.length() - 2);
    return this->dirPath;
}

bool DepthFirstSearch::recursiveDFS(graph_node *current_node)
{
    if (current_node->commingFrom==isInf || current_node->isExamined)
        return false;
    this->sum += current_node->weigth;
    if (current_node->row_index == this->args->end_row && current_node->col_index == this->args->end_col)
        return true;

    current_node->isExamined = true;
    if (current_node->row_index < this->args->rows - 1 && recursiveDFS(&mat[current_node->row_index + 1][current_node->col_index]))
    {
        this->dirPath.insert(0, "Down, ");
        return true;
    }
    if (current_node->col_index < this->args->cols - 1 && recursiveDFS(&mat[current_node->row_index][current_node->col_index + 1]))
    {
        this->dirPath.insert(0, "Right, ");
        return true;
    }
    if (current_node->row_index > 0 && recursiveDFS(&mat[current_node->row_index - 1][current_node->col_index]))
    {
        this->dirPath.insert(0, "Up, ");
        return true;
    }
    if (current_node->col_index > 0 && recursiveDFS(&mat[current_node->row_index][current_node->col_index - 1]))
    {
        this->dirPath.insert(0, "Left, ");
        return true;
    }
    this->sum -= current_node->weigth;

    return false;
}