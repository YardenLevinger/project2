#ifndef DEPTH_FIRST_SEARCH_H
#define DEPTH_FIRST_SEARCH_H

#include "Enums.h"
#include "../Interfaces/Solver.h"
#include "Infrastructure.h"
#include "Searcher.h"
#include <string>
#include <queue>
#include <stack>
#include <iostream>

using namespace std;

namespace Searchers
{

class DepthFirstSearch : public Searcher<graph_node> //public Solver<problem_args, string>
{
private:
  bool recursiveDFS(graph_node *current_node);

public:
  DepthFirstSearch() : Searcher(NULL, NULL) {}
  virtual string solve(problem_args args);
};
} // namespace Searchers

#endif