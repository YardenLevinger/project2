#ifndef BREADTH_FIRST_SEARCH_H
#define BREADTH_FIRST_SEARCH_H

#include "Enums.h"
#include "../Interfaces/Solver.h"
#include "Infrastructure.h"
#include "Searcher.h"
#include <string>
#include <queue>
#include <stack>
#include <iostream>

using namespace std;

namespace Searchers
{
class BreadthFirstSearch : public Searcher<graph_node>//public Solver<problem_args, string>
{
  private:
  static bool compareNodes(graph_node *a, graph_node *b)
  {
    return false;
  }
  static void initialze_neighbor(graph_node *current, graph_node *neighbor, problem_args *args)
  {
    return;
  }

public:
  BreadthFirstSearch():Searcher(&compareNodes,&initialze_neighbor){}
};
} // namespace Searchers

#endif