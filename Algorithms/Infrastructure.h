#ifndef INFRASTRUCTURE_H
#define INFRASTRUCTURE_H
#include "Enums.h"

namespace Searchers
{
class graph_node
{
  public:
    int weigth;
    bool isExamined;
    Direction commingFrom;
    unsigned char row_index;
    unsigned char col_index;

    graph_node()
    {
        this->col_index = 0;
        this->row_index = 0;
        this->weigth = 0;
        this->isExamined = false;
        this->commingFrom = NotSet;
    }
    graph_node(int **mat, unsigned char row, unsigned char col)
    {
        this->col_index = col;
        this->row_index = row;
        this->weigth = mat[row][col];
        this->isExamined = false;
        if (this->weigth != -1)
            this->commingFrom = NotSet;
        else
            this->commingFrom = isInf;
    }

    bool operator<(const graph_node &other) const
    {
        return (this->weigth > other.weigth);
    }
};

typedef struct
{
    int **mat;
    unsigned char rows;
    unsigned char cols;
    unsigned char start_row;
    unsigned char start_col;
    unsigned char end_row;
    unsigned char end_col;
} problem_args;

} // namespace Searchers

#endif